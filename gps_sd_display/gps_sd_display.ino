#include <Wire.h>
#include <ST7032.h>
#include <SoftwareSerial.h>
#include <SPI.h>
#include <TinyGPS++.h>
#include <SD.h>

File sd;
TinyGPSPlus gps;
const int cs = 10; //SDカードのチップセレクト
const int rx=8,tx=9; //GPS受信機の通信線 (逆転注意)
SoftwareSerial ss(rx,tx); //GPSとの通信
ST7032 lcd;
char dayfile[16];
int fileNo =0;


void setup(){
  lcd.begin(8,2); //8x2のLCD
  lcd.setContrast(20); //コントラスト
  lcd.clear(); //液晶画面のクリア
  
  Serial.begin(9600); //みつは通信速度
  ss.begin(9600); //GPS通信速度
  if(!SD.begin()){ //SDカードがない場合
    while(1){
    lcd.print("Not find SD "); //液晶に表示　SDカードを入れてから電源を入れなおす
    delay(350);
    }
  }
  fileName_make(); //ファイルを作る
  SdFile::dateTimeCallback(&dateTime); //ファイルに日付をつける。
}


void loop(){
  sd =SD.open(dayfile,FILE_WRITE); //SDカードを開く
  while(ss.available()>0){ //GPS受信ができている間
    if(gps.encode(ss.read())){ //GPSのデータを読んで
      log_sd(); //このプログラムは下にあります
    }
  }
  sd.close(); //SDカードを閉じる
}


void fileName_make(){ //ファイル名を作る　2018.1.9.の記事参照
String d;
while(1){
d = "Log";
if(fileNo < 10){
d +="00";
}else if(fileNo < 100){
d +="0";
}
d +=fileNo;
d +=".TXT";
d.toCharArray(dayfile,16);
if(!SD.exists(dayfile)) break;
fileNo++;
}
}


void log_sd(){ //緯度と経度をSDカードに書くプログラム
  if(gps.location.isValid()){ //GPSから位置情報を得られていれば
    String n;
    String m;
    n = String(gps.location.lat(),6); //緯度情報を取得
    m ="N";
    m +=n;

// ディスプレイ表示
    lcd.setCursor(0,0); //液晶ディスプレイの上段に
    lcd.print(m); // Nを付けて表示
// SD保存
    sd.print(n); //SDカードに緯度を記録
    sd.print(","); //SDカードに「,」を記録

    n = String(gps.location.lng(),6); //経度情報を取得
    m ="E";
    m +=n;

// ディスプレイ表示
    lcd.setCursor(0,1); //液晶ディスプレイの下段に
    lcd.print(m); //Eを付けて表示
// SD保存    
    sd.println(n); //SDカードに経度を記録
    sd.flush(); //SDカードに書き込んだ記録を保存
  }else{
    lcd.print("Not Find GPS "); //位置情報を得られるまで、表示
    //スイッチを入れてからGPSを捉えるまではこの表示
  }
}


//以下のプログラムでログファイルにつける日付を取得する(2019.1月修正)
void dateTime(uint16_t* date,uint16_t* time){
uint16_t year =(gps.date.year());
uint8_t month =(gps.date.month()),day=(gps.date.day()),
hour=(gps.time.hour()),minute=(gps.time.minute()),second=(gps.time.second());
*date =FAT_DATE(year,month,day);
*time =FAT_DATE(hour,minute,second);
}
