// gps情報をSDカードに保存
/***************************************** ヘッダファイル *****************************************/

#include <SD.h>
#include <SoftwareSerial.h>
#include <TinyGPS++.h>

/***************************************** グローバル変数 *****************************************/

const int chipSelect = 4;
const String file_path = "test.txt";
int flag = 0;
TinyGPSPlus gps;
SoftwareSerial mySerial(8, 9); // TX, RX

/***************************************** 関数 *****************************************/

void printGPSInfo(TinyGPSPlus gps){
  //  最初だけラベル表示
  if(flag == 0){
    Serial.print(F("Date, "));
    Serial.print(F("Time, "));
    Serial.print(F("Latitude, "));
    Serial.print(F("Longitude, "));
    Serial.print(F("Altitude, "));
    Serial.print(F("Speed, "));
    Serial.print(F("Satellites_value, "));
    Serial.println();
    flag = 1;
  }
  // データ表示
  printDate(gps); // YY/MM/DD表記
  Serial.print(F(", "));
  printTime(gps); // HH:MM:SS表記
  Serial.print(F(", "));
  Serial.print(gps.location.lat(), 6);
  Serial.print(F(", "));
  Serial.print(gps.location.lng(), 6);
  Serial.print(F(", "));
  Serial.print(gps.altitude.meters());
  Serial.print(F(", "));
  Serial.print(gps.speed.kmph());
  Serial.print(F(", "));
  Serial.print(gps.satellites.value());
  Serial.println(F(", "));
  delay(1000);
}

// 日付を表示する(YY/MM/DD)
void printDate(TinyGPSPlus gps){
  Serial.print(gps.date.year());
  Serial.print(F("/"));
  Serial.print(gps.date.month());
  Serial.print(F("/"));
  Serial.print(gps.date.day());
}

// 時刻を表示する(HH:MM:SS)
void printTime(TinyGPSPlus gps){
  Serial.print(gps.time.hour());
  Serial.print(F(":"));
  Serial.print(gps.time.minute());
  Serial.print(F(":"));
  Serial.print(gps.time.second());
}

// ラベル表示
void printLabel(File dataFile){
  Serial.print(F("Year, "));
  Serial.print(F("Month, "));
  Serial.print(F("Day, "));
  Serial.print(F("Hour, "));
  Serial.print(F("Minute, "));
  Serial.print(F("Second, "));
  Serial.print(F("Latitude, "));
  Serial.print(F("Longitude, "));
  Serial.print(F("Altitude, "));
  Serial.print(F("Speed, "));
  Serial.println(F("Satellites_value, "));

  dataFile.print("Year, ");
  dataFile.print("Month, ");
  dataFile.print("Day, ");
  dataFile.print("Hour, ");
  dataFile.print("Minute, ");
  dataFile.print("Second, ");
  dataFile.print("Latitude, ");
  dataFile.print("Longitude, ");
  dataFile.print("Altitude, ");
  dataFile.print("Speed, ");
  dataFile.println("Satellites_value, ");
}

// データ書き込み
void writeData(File dataFile){
  printDate(gps); // YY/MM/DD表記
  Serial.print(F(", "));
  printTime(gps); // HH:MM:SS表記
  Serial.print(F(", "));
  Serial.print(gps.location.lat(), 6);
  Serial.print(F(", "));
  Serial.print(gps.location.lng(), 6);
  Serial.print(F(", "));
  Serial.print(gps.altitude.meters());
  Serial.print(F(", "));
  Serial.print(gps.speed.kmph());
  Serial.print(F(", "));
  Serial.print(gps.satellites.value());
  Serial.println(F(", "));

  dataFile.print(gps.date.year());
  dataFile.print(", ");
  dataFile.print(gps.date.month());
  dataFile.print(", ");
  dataFile.print(gps.date.day());
  dataFile.print(", ");
  dataFile.print(gps.time.hour());
  dataFile.print(", ");
  dataFile.print(gps.time.minute());
  dataFile.print(", ");
  dataFile.print(gps.time.second());
  dataFile.print(", ");
  dataFile.print(gps.location.lat(), 6);
  dataFile.print(", ");
  dataFile.print(gps.location.lng(), 6);
  dataFile.print(", ");
  dataFile.print(gps.altitude.meters());
  dataFile.print(", ");
  dataFile.print(gps.speed.kmph());
  dataFile.print(", ");
  dataFile.print(gps.satellites.value());
  dataFile.println(", ");
}


/***************************************** setup() *****************************************/

void setup(){
  Serial.begin(57600);
  Serial.print(F("Initializing SD card..."));
  pinMode(SS, OUTPUT);
  // SDライブラリ初期化
  if (!SD.begin(chipSelect)) {
    Serial.println(F("Card failed, or not present"));
    // 失敗、何もしない
    while(1);
  }
  Serial.println(F("SD OK"));
  // GPSモジュール初期化
  Serial.println(F("Getting GPS info..."));
  while (!Serial) {;}
  Serial.println(F("GPS OK"));
  mySerial.begin(9600);
}

/***************************************** loop() *****************************************/

void loop(){
  File dataFile = SD.open(file_path, FILE_WRITE);
  String date;
  String time;
  if (dataFile){
    while (mySerial.available() > 0){
      char c = mySerial.read();
      gps.encode(c);
      if (gps.location.isUpdated()){
        //  最初だけラベル表示
        if(flag == 0){
          printLabel(dataFile);
          flag = 1;
        }
        // データ書き込み
        writeData(dataFile);
        }
    }
  }
  // ファイルが開けなかったらエラーを出力
  else {
    Serial.println(F("error opening datalog.txt"));
  }
  // 一秒待つ
  delay(1000);
}
