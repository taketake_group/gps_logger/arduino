/*
  Software serial multple serial test

 Receives from the hardware serial, sends to software serial.
 Receives from software serial, sends to hardware serial.

 The circuit:
 * RX is digital pin 10 (connect to TX of other device)
 * TX is digital pin 11 (connect to RX of other device)

 Note:
 Not all pins on the Mega and Mega 2560 support change interrupts,
 so only the following can be used for RX:
 10, 11, 12, 13, 50, 51, 52, 53, 62, 63, 64, 65, 66, 67, 68, 69

 Not all pins on the Leonardo and Micro support change interrupts,
 so only the following can be used for RX:
 8, 9, 10, 11, 14 (MISO), 15 (SCK), 16 (MOSI).

 created back in the mists of time
 modified 25 May 2012
 by Tom Igoe
 based on Mikal Hart's example

 This example code is in the public domain.

 */
#include <SoftwareSerial.h>
#include <TinyGPS++.h>

TinyGPSPlus gps;
SoftwareSerial mySerial(10, 11); // TX, RX

// 日付と時刻を表示する
void printDateTime(TinyGPSPlus gps){
  printDate(gps);
  printTime(gps);
}

// 日付を表示する(YY/MM/DD)
void printDate(TinyGPSPlus gps){
  Serial.print(gps.date.year());
  Serial.print("/");
  Serial.print(gps.date.month());
  Serial.print("/");
  Serial.print(gps.date.day());
}

// 時刻を表示する(HH:MM:SS)
void printTime(TinyGPSPlus gps){
  Serial.print(gps.time.hour());
  Serial.print(":");
  Serial.print(gps.time.minute());
  Serial.print(":");
  Serial.print(gps.time.second());
}

void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(57600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.println("GPS情報取得中");
  // set the data rate for the SoftwareSerial port
  mySerial.begin(9600);
}

void loop() { // run over and over
  while (mySerial.available() > 0){
    char c = mySerial.read();
    gps.encode(c);
    if (gps.location.isUpdated()){
//      Serial.print("DATE=");
//      printDate(gps);
//      Serial.println();
//
//      Serial.print("TIME=");
//      printTime(gps);
//      Serial.println();
      printDateTime(gps);
      Serial.println();
      
      Serial.print("LAT=");
      Serial.println(gps.location.lat(), 6);
      Serial.print("LONG=");
      Serial.println(gps.location.lng(), 6);
      Serial.print("ALT=");
      Serial.println(gps.altitude.meters());
      Serial.print("SPEED(m/s)=");
      Serial.println(gps.speed.mps());
      Serial.print("SATELLITES_VALUE=");
      Serial.println(gps.satellites.value());
      Serial.println();
    }
  }
}
